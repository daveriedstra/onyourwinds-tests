const OPTIONS = {
    PARTICLE_COUNT: 15,
    THRESHOLD: 0.03
}
const AUDIO_SAMPLE_URL = 'https://daveriedstra.com/dev/assets/audio/OnYourWinds-IndividualBreathFiles/Breaths-Complete-Edited-NotStretched/webm/WindE-George-noiseReduced.webm';
const FFT_SIZE = Math.pow(2, 11);
const BIN_COUNT = FFT_SIZE;
const ANALYSIS_INTERVAL = 50; // ms

function randBetween(lo, hi, int = false) {
    const out = Math.random() * (hi - lo) + lo;
    return int ? Math.round(out) : out;
}

function makeParticleContainer() {
    const div = document.createElement('div');
    div.classList.add('container');

    return div;
}

function makeParticle({top, left}) {
    const div = document.createElement('div');
    div.classList.add('particle');

    div.style.top = `${top}px`;
    div.style.left = `${left}px`;
    div.style.opacity = randBetween(0.5, 1);
    div.style.width = div.style.height = `${Math.ceil(Math.random() * 5)}px`;

    return div;
}

/**
 * Translates the element to a new position within its parent element /
 * offsetParent.
 */
function animateParticle(p) {
    const left = p.offsetLeft,
        top = p.offsetTop,
        parentHeight = p.offsetParent.offsetHeight,
        parentWidth = p.offsetParent.offsetWidth;
    let x = randBetween(-1, 1);
    let y = randBetween(-1, 1);
    x = x < 0 ? x * left : x * (parentWidth - left);
    y = y < 0 ? y * top : y * (parentHeight - top);
    p.style.transform = `translate(${x}px, ${y}px)`;
    p.style.opacity = "0";
}

function animateContainer(c) {
    c.classList.add('container-out');
    const removeC = e => {
        c.remove();
        c.removeEventListener('transitionend', removeC);
    }
    c.addEventListener('transitionend', removeC);
}

function addToDom({container, particles}) {
    particles.forEach(p => container.appendChild(p))
    document.body.appendChild(container);
}

function checkboxToggleBodyClass(checkboxId, className) {
    document.getElementById(checkboxId).addEventListener('change', e => {
        if (e.target.checked)
            document.body.classList.add(className)
        else
            document.body.classList.remove(className)
    });
}

function linkNumberInput(inputId, optName) {
    const el = document.getElementById(inputId);
    el.value = OPTIONS[optName];
    el.addEventListener('change', e => {
        OPTIONS[optName] = e.target.value;
        console.log(OPTIONS[optName]);
    })
}

function showParticles() {
    const container = makeParticleContainer(),
        particles = Array.from({length: OPTIONS.PARTICLE_COUNT}).map(() => {
            return makeParticle({
                top: randBetween(0, document.body.offsetHeight, true),
                left: randBetween(0, document.body.offsetWidth, true)
            })
        });

    addToDom({container, particles});
    setTimeout(() => {
        particles.forEach(p => animateParticle(p))
        animateContainer(container);
    }, 10)
}

/**
 * Get noisiness of array of frequency-domain bins
 */
function getNoisiness(arr) {
    // a sample is more noisy if the average amplitude is closer to the sample
    // max across all frequency bins

    const mean = arr.reduce((a, b) => a + b) / arr.length;
    const max = Math.max(...arr);

    return max === 0 || !isFinite(max) ? 1 : 1 - ((mean + 100) / (max + 100));
}

/**
 * Init
 */

linkNumberInput('particle-count', 'PARTICLE_COUNT');
linkNumberInput('threshold', 'THRESHOLD');

if (!navigator.mediaDevices) {
    alert('no mic found -- the page won\'t work :(')
} else {
    navigator.mediaDevices.getUserMedia({
        audio: true,
        video: false
    }).then(stream => {
        const ctx = new AudioContext(),
            analyser = ctx.createAnalyser();
        analyser.fftSize = FFT_SIZE;
        const analyserData = new Float32Array(BIN_COUNT / 2);
        let analyserRunning = false;
        let container = [];

        const startAnalyser = () => {
            analyserRunning = true;
            analyseData();
        }

        const stopAnalyser = () => {
            analyserRunning = false;
            console.log(container);
            container = [];
        }

        const analyseData = () => {
            if (!analyserRunning)
                return;

            window.setTimeout(analyseData, ANALYSIS_INTERVAL)

            analyser.getFloatFrequencyData(analyserData);
            let noisiness = getNoisiness(analyserData);

            container.push(noisiness);
        }

        function play() {
            startAnalyser();
            document.body.classList.remove('paused');
            document.body.classList.add('playing');
        }

        function pause() {
            stopAnalyser();
            document.body.classList.add('paused');
            document.body.classList.remove('playing');
        }

        // connect incoming audio to analyser
        const source = ctx.createMediaStreamSource(stream);
        source.connect(analyser);

        // register body click handler
        document.body.addEventListener('click', e => {
            if (e.target.tagName.toLowerCase() !== 'input') {
                analyserRunning ? pause() : play()
            }
        });

        // start analysis on load
        play();
    }, () => alert('you need to allow the page to use your mic input for this to'
        + ' work\n(this message will be presented more nicely in the final product)')
    );
}
