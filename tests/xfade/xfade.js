function secToTimecode(d) {
    var min = Math.floor(d / 60);
    var minStr = "" + (min < 10 ? 0 : '') + min;
    var sec = Math.floor(d % 60);
    var secStr = "" + (sec < 10 ? 0 : '') + sec;
    var ms = ("" + d).split('.')[1];
    return minStr + ":" + secStr + "." + ms;
}
function getSources() {
    var q = window.location.search.split('?')[1]
        .split('&')
        .map(function (_s) {
        var s = _s.split('=');
        return { key: s[0], value: s[1] || 'true' };
    });
    return q.filter(function (x) { return x.key == 'srcs'; })[0].value.split(',');
}
function enablePlay(vidstats, loadStarted) {
    document.body.classList.remove('loading');
    var loadTime = (performance.now() - loadStarted) / 1000;
    vidstats.push("<p><strong>load time</strong> " + secToTimecode(loadTime));
    var div = document.createElement('div');
    div.innerHTML = vidstats.join('');
    document.body.appendChild(div);
}
var Video = /** @class */ (function () {
    function Video(src) {
        this.src = src;
        this.el = this.makeElement(src);
    }
    Video.prototype.on = function (event, cb) {
        this.el.addEventListener(event, cb);
    };
    Video.prototype.once = function (event, cb) {
        var _el = this.el;
        var _cb = function () {
            cb(arguments);
            _el.removeEventListener(event, _cb);
        };
        this.el.addEventListener(event, _cb);
    };
    Video.prototype.getVidStats = function () {
        var srcParts = this.src.split('/');
        var d = secToTimecode(this.el.duration);
        return "<p><strong>" + srcParts[srcParts.length - 1] + "</strong> (" + this.el.videoWidth + " x\n        " + this.el.videoHeight + ") " + d + "</p>";
    };
    Video.prototype.play = function () {
        this.el.play();
    };
    Video.prototype.pause = function () {
        this.el.pause();
    };
    Video.prototype.makeElement = function (src) {
        var v = document.createElement('video');
        v.setAttribute('preload', 'auto');
        v.setAttribute('muted', 'muted');
        v.setAttribute('loop', 'loop');
        var s = document.createElement('source');
        s.src = src + "?v=" + Math.random();
        s.setAttribute('type', 'video/mp4');
        v.appendChild(s);
        return v;
    };
    return Video;
}());
(function () {
    // make video elements
    var container = document.getElementById('video-container');
    var vidstats = [];
    var now = performance.now();
    var vids = getSources().map(function (s) { return new Video(s); });
    // play toggle
    var paused = true;
    document.getElementById('play-toggle')
        .addEventListener('click', function (e) {
        e.preventDefault();
        vids[vids.length - 1].play();
        window.setInterval(doFadeout, 5 * 1000);
    });
    // vids
    vids.forEach(function (v) {
        container.appendChild(v.el);
        v.once('canplaythrough', function () {
            vidstats.push(v.getVidStats());
            if (vidstats.length == vids.length)
                enablePlay(vidstats, now);
        });
        v.on('waiting', function () { return console.log('buffering...'); });
    });
    // play last vid. after N sec, play second-last vid, fadeout last vid
    var doFadeout = function () {
        // fade last vid
        var topVid = vids.pop();
        topVid.el.classList.add('fadeout');
        vids.unshift(topVid);
        // start second last vid
        var nextUp = vids[vids.length - 1];
        nextUp.play();
        // after fadeout, pause last vid, move to bottom of stack, reset timer
        topVid.once('transitionend', function (e) {
            topVid.pause();
            topVid.el.classList.remove('fadeout');
            container.prepend(topVid.el);
        });
    };
})();
