function secToTimecode(d: number) {
    const min = Math.floor(d / 60);
    const minStr = `${min < 10 ? 0 : ''}${min}`;
    const sec = Math.floor(d % 60);
    const secStr = `${sec < 10 ? 0 : ''}${sec}`;
    const ms = `${d}`.split('.')[1];

    return `${minStr}:${secStr}.${ms}`;
}

function getSources(): string[] {
    const q = window.location.search.split('?')[1]
        .split('&')
        .map(_s => {
            const s = _s.split('=')
            return {key: s[0], value: s[1] || 'true'}
        });
    return q.filter(x => x.key == 'srcs')[0].value.split(',');
}

function enablePlay(vidstats: string[], loadStarted: number) {
    document.body.classList.remove('loading');

    const loadTime = (performance.now() - loadStarted) / 1000;
    vidstats.push(`<p><strong>load time</strong> ${secToTimecode(loadTime)}`);

    const div = document.createElement('div');
    div.innerHTML = vidstats.join('');
    document.body.appendChild(div);
}


class Video {
    src: string;
    el: HTMLVideoElement;

    constructor(src: string) {
        this.src = src;
        this.el = this.makeElement(src);
    }

    on(event: string, cb: (...any) => void) {
        this.el.addEventListener(event, cb);
    }

    once(event: string, cb: (...any) => void) {
        const _el = this.el;
        const _cb = function() {
            cb(arguments);
            _el.removeEventListener(event, _cb);
        }
        this.el.addEventListener(event, _cb);
    }

    getVidStats(): string {
        const srcParts = this.src.split('/');
        const d = secToTimecode(this.el.duration);
        return `<p><strong>${srcParts[srcParts.length - 1]}</strong> (${this.el.videoWidth} x
        ${this.el.videoHeight}) ${d}</p>`
    }

    play() {
        this.el.play();
    }

    pause() {
        this.el.pause();
    }

    private makeElement(src: string): HTMLVideoElement {
        const v = document.createElement('video');
        v.setAttribute('preload', 'auto');
        v.setAttribute('muted', 'muted');
        v.setAttribute('loop', 'loop');

        const s = document.createElement('source');
        s.src = `${src}?v=${Math.random()}`;
        s.setAttribute('type', 'video/mp4');

        v.appendChild(s);
        return v;
    }
}

(function() {
    // make video elements
    const container = document.getElementById('video-container')
    const vidstats: string[] = [];
    const now = performance.now();
    const vids: Video[] = getSources().map(s => new Video(s));

    // play toggle
    let paused = true;
    document.getElementById('play-toggle')!
        .addEventListener('click', e => {
            e.preventDefault();
            vids[vids.length - 1].play();
            window.setInterval(doFadeout, 5*1000);
        });

    // vids
    vids.forEach(v => {
        container!.appendChild(v.el);
        v.once('canplaythrough', () => {
            vidstats.push(v.getVidStats());
            if (vidstats.length == vids.length)
                enablePlay(vidstats, now);
        });
        v.on('waiting', () => console.log('buffering...'));
    });

    // play last vid. after N sec, play second-last vid, fadeout last vid
    const doFadeout = () => {
        // fade last vid
        let topVid = vids.pop()!;
        topVid.el.classList.add('fadeout');
        vids.unshift(topVid);

        // start second last vid
        let nextUp = vids[vids.length - 1]!;
        nextUp.play();

        // after fadeout, pause last vid, move to bottom of stack, reset timer
        topVid.once('transitionend', e => {
            topVid.pause();
            topVid.el.classList.remove('fadeout');
            container!.prepend(topVid.el);
        });
    }
})()
