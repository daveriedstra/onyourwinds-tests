const PARTICLE_COUNT = 200;
const 
    OUTLINE_CLASS = 'outline',
    GRAVITY_CLASS = 'gravity',
    CLIPPING_CLASS = 'clipping',
    CIRCLE_CLASS = 'circle';

function randBetween(lo, hi, int = false) {
    const out = Math.random() * (hi - lo) + lo;
    return int ? Math.round(out) : out;
}

function makeParticleContainer() {
    const div = document.createElement('div');
    div.classList.add('container');

    return div;
}

function makeParticle({top, left}) {
    const div = document.createElement('div');
    div.classList.add('particle');

    div.style.top = `${top}px`;
    div.style.left = `${left}px`;
    div.style.opacity = randBetween(0.5, 1);
    div.style.width = div.style.height = `${Math.ceil(Math.random() * 5)}px`;

    return div;
}

/**
 * Translates the element to a new position within its parent element /
 * offsetParent.
 */
function animateParticle(p) {
    const left = p.offsetLeft,
        top = p.offsetTop,
        parentHeight = p.offsetParent.offsetHeight,
        parentWidth = p.offsetParent.offsetWidth;
    let x = randBetween(-1, 1);
    let y = randBetween(-1, 1);
    x = x < 0 ? x * left : x * (parentWidth - left);
    y = y < 0 ? y * top : y * (parentHeight - top);
    p.style.transform = `translate(${x}px, ${y}px)`;
}

function animateContainer(c) {
    c.classList.add('container-out');
    const removeC = e => {
        c.remove();
        c.removeEventListener('transitionend', removeC);
    }
    c.addEventListener('transitionend', removeC);
}

function addToDom({container, particles}) {
    particles.forEach(p => container.appendChild(p))
    document.body.appendChild(container);
}

function checkboxToggleBodyClass(checkboxId, className) {
    document.getElementById(checkboxId).addEventListener('change', e => {
        if (e.target.checked)
            document.body.classList.add(className)
        else
            document.body.classList.remove(className)
    });
}

/**
 * Init
 */

checkboxToggleBodyClass('outline-toggle', OUTLINE_CLASS);
checkboxToggleBodyClass('gravity-toggle', GRAVITY_CLASS);
checkboxToggleBodyClass('clipping-toggle', CLIPPING_CLASS);
checkboxToggleBodyClass('circle-toggle', CIRCLE_CLASS);

document.body.addEventListener('click', e => {
    const pos = {top: e.pageY, left: e.pageX};
    const container = makeParticleContainer(),
        particles = Array.from({length: PARTICLE_COUNT}).map(() => makeParticle(pos));

    addToDom({container, particles});
    setTimeout(() => {
        particles.forEach(p => animateParticle(p))
        animateContainer(container);
    }, 10)
});
