const OPTIONS = {
    PARTICLE_COUNT: 15,
    THRESHOLD: 0.02
}
const AUDIO_SAMPLE_URL = 'https://daveriedstra.com/dev/assets/audio/OnYourWinds-IndividualBreathFiles/Breaths-Complete-Edited-NotStretched/webm/WindE-George-noiseReduced.webm';
const FFT_SIZE = Math.pow(2, 11);
const BIN_COUNT = FFT_SIZE;
const ANALYSIS_INTERVAL = 50; // ms

function randBetween(lo, hi, int = false) {
    const out = Math.random() * (hi - lo) + lo;
    return int ? Math.round(out) : out;
}

function makeParticleContainer() {
    const div = document.createElement('div');
    div.classList.add('container');

    return div;
}

function makeParticle({top, left}) {
    const div = document.createElement('div');
    div.classList.add('particle');

    div.style.top = `${top}px`;
    div.style.left = `${left}px`;
    div.style.opacity = randBetween(0.5, 1);
    div.style.width = div.style.height = `${Math.ceil(Math.random() * 5)}px`;

    return div;
}

/**
 * Translates the element to a new position within its parent element /
 * offsetParent.
 */
function animateParticle(p) {
    const left = p.offsetLeft,
        top = p.offsetTop,
        parentHeight = p.offsetParent.offsetHeight,
        parentWidth = p.offsetParent.offsetWidth;
    let x = randBetween(-1, 1);
    let y = randBetween(-1, 1);
    x = x < 0 ? x * left : x * (parentWidth - left);
    y = y < 0 ? y * top : y * (parentHeight - top);
    p.style.transform = `translate(${x}px, ${y}px)`;
    p.style.opacity = "0";
}

function animateContainer(c) {
    c.classList.add('container-out');
    const removeC = e => {
        c.remove();
        c.removeEventListener('transitionend', removeC);
    }
    c.addEventListener('transitionend', removeC);
}

function addToDom({container, particles}) {
    particles.forEach(p => container.appendChild(p))
    document.body.appendChild(container);
}

function checkboxToggleBodyClass(checkboxId, className) {
    document.getElementById(checkboxId).addEventListener('change', e => {
        if (e.target.checked)
            document.body.classList.add(className)
        else
            document.body.classList.remove(className)
    });
}

function linkNumberInput(inputId, optName) {
    const el = document.getElementById(inputId);
    el.value = OPTIONS[optName];
    el.addEventListener('change', e => {
        OPTIONS[optName] = e.target.value;
        console.log(OPTIONS[optName]);
    })
}

function showParticles() {
    const container = makeParticleContainer(),
        particles = Array.from({length: OPTIONS.PARTICLE_COUNT}).map(() => {
            return makeParticle({
                top: randBetween(0, document.body.offsetHeight, true),
                left: randBetween(0, document.body.offsetWidth, true)
            })
        });

    addToDom({container, particles});
    setTimeout(() => {
        particles.forEach(p => animateParticle(p))
        animateContainer(container);
    }, 10)
}
/**
 * Get the RMS value for an array
 */
function getRms(arr) {
    const sqSum = arr.map(i => i * i).reduce((a, b) => a + b);
    return Math.sqrt(sqSum / arr.length);
}

/**
 * Init
 */

linkNumberInput('particle-count', 'PARTICLE_COUNT');
linkNumberInput('threshold', 'THRESHOLD');

const h = new Howl({
    src: [AUDIO_SAMPLE_URL]
});
const ctx = Howler.ctx,
    analyser = ctx.createAnalyser();
analyser.fftSize = FFT_SIZE;
const analyserData = new Float32Array(BIN_COUNT);
Howler.masterGain.connect(analyser);

let analyserRunning = false;

const startAnalyser = () => {
    analyserRunning = true;
    analyseData();
}

const stopAnalyser = () => {
    analyserRunning = false;
}

const analyseData = () => {
    if (!analyserRunning)
        return;

    window.setTimeout(analyseData, ANALYSIS_INTERVAL)

    analyser.getFloatTimeDomainData(analyserData);
    let rmsAmp = getRms(analyserData);

    if (rmsAmp >= OPTIONS.THRESHOLD)
        showParticles()
}

function play() {
    h.play();
    startAnalyser();
    document.body.classList.remove('paused');
    document.body.classList.add('playing');
}

function pause() {
    h.pause();
    stopAnalyser();
    document.body.classList.add('paused');
    document.body.classList.remove('playing');
}

h.on('end', () => {
    stopAnalyser();
    document.body.classList.remove('playing')
});

h.once('load', () => document.body.addEventListener('click', e => {
    if (e.target.tagName.toLowerCase() !== 'input')
        h.playing() ? pause() : play()
}));
