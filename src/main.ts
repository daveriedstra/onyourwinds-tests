import { VideoPool, Video, Pool, AudioPool, MediaPool, Asset } from 'pool-time';
// import { VideoPool, Video, Pool, AudioPool, MediaPool, Asset } from '../../pool-time/src/index';
import { pipe, merge } from 'rxjs';
import { take, last } from 'rxjs/operators';

const SRCS_ROOT = 'https://daveriedstra.com/dev/assets/video/slowed/';
//const SRCS_ROOT = '../../videotest/assets/video/slowed/';
// const SRCS_SIZES = [ '1920', '1280', '720', '480' ];
const SRCS_SIZES = [ '1080', '720', '480', '360', '240', '144' ];
const FORMATS = [
    // { ext: 'webm', type: 'video/webm' }, // assets not yet available
    { ext: 'MP4', type: 'video/mp4' }
];
const SRC_BG = 'C0003';
const SRCS = [
    'C0006-0.25x',
    'C0017-0.25x',
    'C0040-0.25x'
]
const $ = document.querySelectorAll.bind(document);
let EL_VID_CONTAINER: HTMLDivElement;
let EL_CIRCLES_CONTAINER: HTMLDivElement;
const VIDEO_DISABLED = window.location.search.includes("novideo");

function makeVideoAsset(src, size): Asset {
    let out: Asset = {
        srcs: [],
        formats: []
    }

    FORMATS.forEach(({ext, type}) => {
        let fname = `${src}-${size}.${ext}`;
        out.srcs.push(`${SRCS_ROOT}${fname}`);
        out.formats.push(type);
    });

    return out;
}

function makeOverlayPool(): VideoPool {
    EL_VID_CONTAINER = $('#video-container')[0];
    const overlayPool = new VideoPool({
        duration: () => (Math.random() * 5 + 3) * 6000,
        //dropCount: () => Math.round(Math.random() * 2) * 1,
        dropCount: 2,
        loop: true,
        assets: SRCS.map(s => makeVideoAsset(s, SRCS_SIZES[0]))
            .map(a => {
                a.seek = duration => duration * Math.random() * 0.5;
                return a;
            })
    });

    overlayPool.start$.subscribe(() => {
        overlayPool.queue.forEach(v => {
            v.el.classList.add('out');
            EL_VID_CONTAINER.appendChild(v.el);
        })
    });

    // play a short video overlay
    overlayPool.play$.subscribe(nextVideo => {
        nextVideo.once('play', e => e.target.classList.remove('out'))

        nextVideo.after(5, e => {
            nextVideo.once('transitionend', () => overlayPool.destroyVideo(nextVideo))
            nextVideo.el.classList.add('out');
        })
    });
    return overlayPool;
}

function makeCirclesPool(): VideoPool {
    EL_CIRCLES_CONTAINER = $('#circles-container')[0];
    const circlesPool = new VideoPool({
        duration: () => (Math.random() * 5 + 5) * 4000,
        dropDistributionWeight: 2,
        dropCount: () => Math.round(Math.random() * 7) + 5,
        loop: true,
        assets: SRCS.map(s => makeVideoAsset(s, SRCS_SIZES[5]))
            .map(a => {
                a.seek = duration => duration * Math.random() * 0.5;
                return a;
            })
    });

    circlesPool.start$.subscribe(() => {
        circlesPool.queue.forEach(v => {
            v.el.classList.add('out');
            EL_CIRCLES_CONTAINER.appendChild(v.el);
            v.el.style.top = `${Math.random() * 100}%`;
            v.el.style.left = `${Math.random() * 100}%`;
        })
    });

    // play a short video overlay
    circlesPool.play$.subscribe(nextVideo => {
        nextVideo.once('play', e => e.target.classList.remove('out'))

        nextVideo.after(5, e => {
            nextVideo.once('transitionend', () => circlesPool.destroyVideo(nextVideo));
            nextVideo.el.classList.add('out');
        })
    });
    return circlesPool;
}

function makeAudioPools(): AudioPool[] {
    const dir = 'https://daveriedstra.com/dev/assets/audio/OnYourWinds-IndividualBreathFiles/';
    const srcsUnstretched = [
        "aaAilaPlain3times",
        "aaKnutPlainonce",
        "aaRosanna-namePlainNR",
        "aaTamsinBitPlain",
        "E-Wende-Breath1EQ",
        "laura-wind-2",
        "WindE-George-noiseReduced"
    ].map(s => ({
        srcs: ['webm', 'mp3'].map(format => `${dir}Breaths-Complete-Edited-NotStretched/${format}/${s}.${format}`),
        seek: duration => Math.random() * duration
    }));
    const srcsStretched = [
        "aaAilax10EQ_11\"",
        "aaAliceStretchLowGustM_40\"",
        "aaBenjaminx60+EQ",
        "aaKnutStretchx12+EQ",
        "aaRosannax20",
        "aaTamsinx30(hiss)",
        "JerryPstretch2'15",
        "LauraStretchx10",
        "LauraStretchx5",
        "WindStretchGeorge3-talking"
    ].map(s => ({
        srcs: ['webm', 'mp3'].map(format => `${dir}Stretched-Breath_Segments/${format}/${s}.${format}`)
    }));

    // unstretched are sporadic and sparse
    const unstretchedPool = new AudioPool({
        duration: () => (Math.round(Math.random() * 10) + 10) * 1000,
        dropCount: () => Math.round(Math.random() * 2), // 0 - 2
        assets: srcsUnstretched,
        loop: true
    });

    unstretchedPool.play$.subscribe(({id, howl}) => {
        howl.volume(0, id);
        howl.fade(0, Math.random() * 0.2 + 0.8, 500, id)
        howl.stereo(0.9 * (Math.random() * 2 - 1), id)
    })

    // stretched makes some kind of pad
    const stretchedPool = new AudioPool({
        duration: () => (Math.round(Math.random() * 30) + 30) * 1000,
        dropCount: 3,
        dropDistributionWeight: 2,
        assets: srcsStretched,
        loop: true,
        cleanEvery: 3
    });

    return [unstretchedPool, stretchedPool]
}

function makePools(): MediaPool[] {
    const out: MediaPool[] = [...makeAudioPools()];

    if (!VIDEO_DISABLED) {
        out.push(...[
            makeOverlayPool(),
            makeCirclesPool()
        ])
    }

    return out;
}

let analyser: AnalyserNode,
    analyserData: Float32Array,
    analyserBinCount: number;

function initAnalyzer() {
    const ctx = Howler.ctx;
    analyser = ctx.createAnalyser();
    analyser.fftSize = Math.pow(2, 10)
    // I believe we should use the frequencyBinCount if in frequency domain
    // only, as it's half the sample length per Nyquist. However, MDN docs
    // not very clear on this.
    // analyserBinCount = analyser.frequencyBinCount; // <- if Frequency domain
    analyserBinCount = analyser.fftSize; // <- if time domain
    analyserData = new Float32Array(analyserBinCount);
    Howler.masterGain.connect(analyser);

    analyzerCb();
}

/**
 * Called each animationFrame. There's a better way to structure this.
 */
let frame_count = 0;
const EVERY = 2;
function analyzerCb() {
    // only run cb if not paused
    if (!paused)
        window.requestAnimationFrame(analyzerCb); // <- recursion warning!

    // only do heavy lifting every EVERY frames
    frame_count = (frame_count + 1) % EVERY;
    if (frame_count !== 0)
        return;

    // analyser.getByteFrequencyData(analyserData);
    analyser.getFloatTimeDomainData(analyserData);

    // do stuff with analyserData array
    const v = getRms(analyserData) * 3;
    EL_VID_CONTAINER.style.opacity = `${v}`;
    EL_CIRCLES_CONTAINER.style.opacity = `${v}`;
}

/**
 * Get the RMS value for an array
 */
function getRms(arr: Float32Array): number {
    const sqSum = arr.map(i => i * i).reduce((a, b) => a + b);
    return Math.sqrt(sqSum / arr.length);
}

let paused = false;
function init() {
    // get elements
    const EL_BG_CONTAINER = $('#background-container')[0];
    const EL_PLAY_LINK = $('#start')[0];
    const EL_PAUSE_LINK = $('#pause')[0];
    let BG_VID: Video;

    // make bg video
    if (!VIDEO_DISABLED) {
        BG_VID = new Video(makeVideoAsset(SRC_BG, SRCS_SIZES[0]));
        EL_BG_CONTAINER.appendChild(BG_VID.el);
    }

    // make pools
    const pools = makePools();
    pools.forEach(p => {
        p.drop$.subscribe(() => console.log('drop'))
    })
    const preload$ = pools.map(p => p.preloadAll())
    let preloaded = false;

    let x = 0;
    EL_PLAY_LINK.classList.add('disabled')
    merge(preload$)
    //.pipe(last())
    .subscribe(() => {
        console.log('loaded x', x)
        x++;
        EL_PLAY_LINK.classList.remove('disabled')
    })

    // play
    EL_PLAY_LINK.addEventListener('click', e => {
        pools.forEach(p => p.start());
        if (!VIDEO_DISABLED)
            BG_VID.playWhenReady();
        e.target.classList.add('hidden');
        document.body.classList.add('playing')
        EL_PAUSE_LINK.classList.remove('hidden');
        if (!VIDEO_DISABLED)
            initAnalyzer();
    });

    // pause toggle
    EL_PAUSE_LINK.addEventListener('click', e => {
        paused = !paused;
        pools.forEach(p => paused ? p.pause() : p.resume());
        if (!VIDEO_DISABLED)
            paused ? BG_VID.pause() : BG_VID.play();
        e.target.innerText = paused ? 'resume' : 'pause';
        document.body.classList[paused ? 'remove' : 'add']('playing')
        if (!paused && !VIDEO_DISABLED)
            analyzerCb()
    });
}

init();
